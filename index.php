<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>Php_PracticeCRUDsystem</title>
		<link href="css/index_style.css" type="text/css" rel="stylesheet">
		<link href="css/dropdown_menu.css" type="text/css" rel="stylesheet">
		<link href="css/content_carton.css" type="text/css" rel="stylesheet">
	
	</head>
	<body>
		<div class="wrapper">
			<header>
				<div id="left">@Jan.2015</div>
				<div id="right">

<!-- DropDown Menu Code Start here --------------->
		<div id='cssmenu'>
		<ul>
		    <li class='active has-sub'><a href='index.php'><span>Home</span></a></li>
		    <li class='has-sub'><a href='#'><span>Insert</span></a>
			<ul>
				<li class="has-sub"><a href="insert_registration.php"><span>Registration</span></a></li>
				<li class="has-sub"><a href="insert_result.php"><span>Insert Result</span></a></li>
			</ul>
		   </li>
		    <li class='has-sub'><a href='#'><span>View</span></a>
			<ul>
				<li class="has-sub"><a href="view_all_student.php"><span>All Student</span></a></li>
				<li class="has-sub"><a href="search_student.php"><span>Search Student</span></a></li>
				<li class="has-sub"><a href="search_result.php"><span>Search Result</span></a></li>
				<li class="has-sub"><a href="view_all_result.php"><span>All Result</span></a></li>
			</ul>
		   </li>
		   <li class='last'><a href='#'><span>Contact Us</span></a></li>
		</ul>
		</div>
<!-- DropDown Menu Code End here --------------->
</div>
</header>
			
<!-- Content field code start from here ...-->

			<div id="content">
				<br/><br/>
				<br/><br/>
			<center><h1> Welcome to Home Page...</h1></center>
<!-- carton code from here-->


			<div class="evil">

				<ul class="evil_hair">
					<li class="eh1"></li>
					<li class="eh2"></li>
					<li class="eh3"></li>
					<li class="eh4"></li>
					<li class="eh5"></li>
				</ul>

				<div class="eyes">
					<div class="eye_animate"></div>

					<div class="glasses"></div>
						<div class="white_part">
							<div class="brown_eye">
								<span class="black_part"></span>
							</div>
						</div>
				</div>

				<div class="black_tie">
					<span class="right_tie">
						<div class="top_tie"></div>
						<div class="down_tie"></div>
					</span>
					<span class="left_tie">
						<div class="top_tie"></div>
						<div class="down_tie"></div>
					</span>
				</div>

				<div class="evil_mouth">
					<span class="evil_teeth"></span>
					<span class="evil_teeth1"></span>
				</div>

				<div class="clothes">
					<div class="right_shirt evil_right_shirt"></div>
					<div class="right_shirt evil_left_shirt"></div>
					<div class="evil_bottom"></div>
					<div class="evil_logo"><p>M</p></div>
				</div>

				<div class="legs">
					<div class="evil_right_leg"></div>
					<div class="evil_right_leg evil_left_leg"></div>
					<div class="shoes evil_right_shoes"><span class="small_shoes"></span></div>
					<div class="shoes evil_left_shoes"><span class="small_shoes"></span></div>
				</div>


				<div class="evil_hand">
					<div class="evil_rh">
						<span class="evil_gloves_rh"></span>
					</div>
					<div class="evil_lh">
						<span class="evil_gloves_lh"></span>
					</div>
				</div>




			
<!-- carton code end from here-->			
			
<br/><br/>
</div>
			
<!--Content field code end from here-->

		</div>
<!-- Footer start from here ----->
		<div class="footer"> .2015@Copyright by-Islam Hossain</div>
	</body>
</html>