<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>Php_Practice01_Jan_2015</title>
		<link href="css/index_style.css" type="text/css" rel="stylesheet">
		<link href="css/dropdown_menu.css" type="text/css" rel="stylesheet">
		<link href="css/content_carton.css" type="text/css" rel="stylesheet">
		<link href="css/insert_registration_style.css" type="text/css" rel="stylesheet">
	
	</head>
	<body>
		<div class="wrapper">
<!-- header start from here-->
<header>
	<div id="left">@Jan.2015</div>
	<div id="right">

		<!-- DropDown Menu Code Start here --------------->
		<div id='cssmenu'>
		<ul>
		    <li class='active has-sub'><a href='index.php'><span>Home</span></a></li>
		    <li class='has-sub'><a href='#'><span>Insert</span></a>
			<ul>
				<li class="has-sub"><a href="insert_registration.php"><span>Registration</span></a></li>
				<li class="has-sub"><a href="insert_result.php"><span>Insert Result</span></a></li>
			</ul>
		   </li>
		    <li class='has-sub'><a href='#'><span>View</span></a>
			<ul>
				<li class="has-sub"><a href="view_all_student.php"><span>All Student</span></a></li>
				<li class="has-sub"><a href="search_student.php"><span>Search Student</span></a></li>
				<li class="has-sub"><a href="search_result.php"><span>Search Result</span></a></li>
				<li class="has-sub"><a href="view_all_result.php"><span>All Result</span></a></li>
			</ul>
		   </li>
		   <li class='last'><a href='#'><span>Contact Us</span></a></li>
		</ul>
		</div>
		<!-- DropDown Menu Code End here --------------->
	</div>
</header>
<!-- header code end from here-->
			
<!-- Content field code start from here ...-->

			<div id="content">
				
				<center><h2> View All Student...</h2></center>
<br/>
<!------------------------------------------------------------------------>
<!------------------------- Form code from here--------------------------->
<!------------------------------------------------------------------------>

<center><h1>Under Construction</h1></center>

	
<!------------------------------------------------------------------------>
<!------------------------- Form code end from here----------------------->
<!------------------------------------------------------------------------>		
</div>
			
<!--Content field code end from here-->

		</div>
<!-- Footer start from here ----->
		<div class="footer">@Copyright_Jan_2015 by-Islam Hossain</div>
	</body>
</html>