<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>Php_Practice01_Jan_2015</title>
		<link href="css/index_style.css" type="text/css" rel="stylesheet">
		<link href="css/dropdown_menu.css" type="text/css" rel="stylesheet">
		<link href="css/content_carton.css" type="text/css" rel="stylesheet">
		<link href="css/insert_registration_style.css" type="text/css" rel="stylesheet">
	
	</head>
	<body>
		<div class="wrapper">
<!-- header start from here-->
<header>
	<div id="left">@Jan.2015</div>
	<div id="right">

		<!-- DropDown Menu Code Start here --------------->
		<div id='cssmenu'>
		<ul>
		    <li class='active has-sub'><a href='index.php'><span>Home</span></a></li>
		    <li class='has-sub'><a href='#'><span>Insert</span></a>
			<ul>
				<li class="has-sub"><a href="insert_registration.php"><span>Registration</span></a></li>
				<li class="has-sub"><a href="insert_result.php"><span>Insert Result</span></a></li>
			</ul>
		   </li>
		    <li class='has-sub'><a href='#'><span>View</span></a>
			<ul>
				<li class="has-sub"><a href="view_all_student.php"><span>All Student</span></a></li>
				<li class="has-sub"><a href="search_student.php"><span>Search Student</span></a></li>
				<li class="has-sub"><a href="search_result.php"><span>Search Result</span></a></li>
				<li class="has-sub"><a href="view_all_result.php"><span>All Result</span></a></li>
			</ul>
		   </li>
		   <li class='last'><a href='#'><span>Contact Us</span></a></li>
		</ul>
		</div>
		<!-- DropDown Menu Code End here --------------->
	</div>
</header>
<!-- header code end from here-->
			
<!-- Content field code start from here ...-->

			<div id="content">
				
				<center><h2> Please a Student Result...</h2></center>
<br/>
<!------------------------------------------------------------------------>
<!--------------------------Form code from here--------------------------->
<!------------------------------------------------------------------------>

<form method="POST" action="php/insert_result_data.php">
<div id="form_row"><div id="data_lebel">Student Roll</div><div id="input_data"><input type="text" name="student_roll_result"></div></div>
<div id="form_row"><div id="data_lebel">Student class</div><div id="input_data"><input type="text" name="student_class_result"></div></div>

<div id="form_row"><div id="data_lebel">Code 1</div><div id="input_data"><input type="text" name="student_class_code1"></div></div>
<div id="form_row"><div id="data_lebel">Code 2</div><div id="input_data"><input type="text" name="student_class_code2"></div></div>
<div id="form_row"><div id="data_lebel">Code 3</div><div id="input_data"><input type="text" name="student_class_code3"></div></div>
<div id="form_row"><div id="data_lebel">Code 4</div><div id="input_data"><input type="text" name="student_class_code4"></div></div>
<div id="form_row"><div id="data_lebel">Code 5</div><div id="input_data"><input type="text" name="student_class_code5"></div></div>
<div id="form_row"><div id="data_lebel">Code 6</div><div id="input_data"><input type="text" name="student_class_code6"></div></div>
<div id="form_row"><div id="data_lebel"></div><div id="input_data"><input type="submit" value="Insert"></div></div>
</form>	
<!------------------------------------------------------------------------>
<!--------------------------Form code end from here----------------------->
<!------------------------------------------------------------------------>		
</div>
			
<!--Content field code end from here-->

		</div>
<!-- Footer start from here ----->
		<div class="footer">@Copyright_Jan_2015 by-Islam Hossain</div>
	</body>
</html>